﻿using LVL2_ASPNet_MVC_03.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_03.Controllers
{
    public class CustomerController : Controller
    {
        db_customer2Entities dbModel = new db_customer2Entities();
        // GET: Customer
        public ActionResult Index()
        {
            return View(dbModel.tbl_customer.ToList());
        }

        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {
            return View(dbModel.tbl_customer.Where(x => x.ID == id).FirstOrDefault());
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(tbl_customer customer)
        {
            using (var transactions = dbModel.Database.BeginTransaction())
            {
                try
                {
                    // TODO: Add insert logic here
                    dbModel.tbl_customer.Add(customer);
                    dbModel.SaveChanges();
                    transactions.Commit();
                    return RedirectToAction("Index");
                }
                catch (Exception errMsg)
                {
                    transactions.Rollback();
                    ViewBag.Error = errMsg.Message;
                    return View();
                }
            }
                
        }

        // GET: Customer/Edit/5
        public ActionResult Edit(int id)
        {
            return View(dbModel.tbl_customer.Where(x => x.ID == id).FirstOrDefault());
        }

        // POST: Customer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, tbl_customer customer)
        {
            try
            {
                // TODO: Add update logic here
                dbModel.Entry(customer).State = EntityState.Modified;
                dbModel.SaveChanges();
                return RedirectToAction("Index");
            }
            catch 
            {
      
                return View();
            }
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(int id)
        {
            return View(dbModel.tbl_customer.Where(x => x.ID == id).FirstOrDefault());
        }

        // POST: Customer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, tbl_customer customer)
        {
            try
            {
                // TODO: Add delete logic here
                customer = dbModel.tbl_customer.Where(x => x.ID == id).FirstOrDefault();//Delete from tbl_customer where id==x
                dbModel.tbl_customer.Remove(customer);
                dbModel.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
